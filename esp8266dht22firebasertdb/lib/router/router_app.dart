import 'package:esp8266dht22firebasertdb/modules/binding/home_binding.dart';
import 'package:esp8266dht22firebasertdb/modules/view/home.dart';
import 'package:esp8266dht22firebasertdb/router/router_page.dart';
import 'package:get/get.dart';

class AppPages {
  static final pages = [
    GetPage(
      name: homePageViewRoute, 
      page: () => HomePageView(),
      bindings: [HomeBinding()]
    )
  ];
}