import 'package:flutter/material.dart';
import 'package:esp8266dht22firebasertdb/modules/controller/realtime_database_controller.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

class HomePageView extends GetView<RealtimeDatabaseController> {
  final RealtimeDatabaseController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text(
            'ESP8266 DHT22 \nMonitoring App',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 15
            ),
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 8,
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.all(16),
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.only(top: 8),
                height: MediaQuery.of(context).size.height / 2.4,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.1),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3)
                    )
                  ]
                ),
                child: Obx(() => SfRadialGauge(
                  title: const GaugeTitle(text: 'Temperature'),
                  axes: <RadialAxis>[
                    RadialAxis(
                      minimum: 0, 
                      maximum: 35, 
                      ranges: <GaugeRange>[
                        GaugeRange(startValue: 0, endValue: 25, color: Colors.green),
                        GaugeRange(startValue: 25, endValue: 30, color: Colors.orange),
                        GaugeRange(startValue: 30, endValue: 35, color: Colors.red)
                      ], 
                      pointers: <GaugePointer>[
                        NeedlePointer(
                          value: controller.temperature
                        )
                      ], 
                      annotations: <GaugeAnnotation>[
                        GaugeAnnotation(
                          widget: Text(
                            controller.temperature.toString() + '°C',
                            style: const TextStyle(
                              fontSize: 25, 
                              fontWeight: FontWeight.bold
                            )
                          ),
                          angle: 90,
                          positionFactor: 0.5
                        )
                      ]
                    )
                  ]
                )),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8),
                padding: const EdgeInsets.only(top: 8),
                height: MediaQuery.of(context).size.height / 2.4,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.1),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3)
                    )
                  ]
                ),
                child: Obx(() => SfRadialGauge(
                  title: const GaugeTitle(text: 'Humidity'),
                  axes: <RadialAxis>[
                    RadialAxis(
                      minimum: 0, 
                      maximum: 100, 
                      ranges: <GaugeRange>[
                        GaugeRange(startValue: 0, endValue: 25, color: Colors.red),
                        GaugeRange(startValue: 25, endValue: 50, color: Colors.orange),
                        GaugeRange(startValue: 50, endValue: 75, color: Colors.lightGreen),
                        GaugeRange(startValue: 75, endValue: 100, color: Colors.green)
                      ], 
                      pointers: <GaugePointer>[
                        NeedlePointer(
                          value: controller.humidity
                        )
                      ], 
                      annotations: <GaugeAnnotation>[
                        GaugeAnnotation(
                          widget: Text(
                            controller.humidity.toString() + '%',
                            style: const TextStyle(
                              fontSize: 25, 
                              fontWeight: FontWeight.bold
                            )
                          ),
                          angle: 90,
                          positionFactor: 0.5
                        )
                      ]
                    )
                  ]
                )),
              )
            ],
          ),
        ),
      ),
    );
  }
}