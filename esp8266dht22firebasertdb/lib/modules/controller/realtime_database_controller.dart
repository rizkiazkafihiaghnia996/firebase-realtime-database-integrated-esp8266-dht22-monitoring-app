import 'package:firebase_database/firebase_database.dart';
import 'package:get/get.dart';

class RealtimeDatabaseController extends GetxController {
  late DatabaseReference dbRef;

  @override
  void onInit() {
    dbRef = FirebaseDatabase.instance.ref();
    humidityChange();
    temperatureChange();
    super.onInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  RxDouble _temperature = 0.0.obs;
  RxDouble _humidity = 0.0.obs;

  double get temperature => _temperature.value;
  double get humidity => _humidity.value;

  set temperature(double temperature) => _temperature.value = temperature;
  set humidity(double humidity) => _humidity.value = humidity;

  void humidityChange() {
    dbRef.child('ESP8266_APP').child('HUMIDITY').onValue.listen((event) { 
      print(event.snapshot.value);
      Object? data = event.snapshot.value;
      humidity = double.parse(data.toString());
    });
  }

  void temperatureChange() {
    dbRef.child('ESP8266_APP').child('TEMPERATURE').onValue.listen((event) { 
      print(event.snapshot.value);
      Object? data = event.snapshot.value;
      temperature = double.parse(data.toString());
    });
  }
}