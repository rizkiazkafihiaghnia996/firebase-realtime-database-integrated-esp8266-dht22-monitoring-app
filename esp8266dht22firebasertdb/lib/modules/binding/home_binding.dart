import 'package:esp8266dht22firebasertdb/modules/controller/realtime_database_controller.dart';
import 'package:get/get.dart';

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RealtimeDatabaseController>(() => RealtimeDatabaseController());
  }
}